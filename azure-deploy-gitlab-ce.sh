az group create -n gitlab
az network nsg create -g gitlab -n GitlabNSG
az network nsg rule create --nsg-name GitlabNSG -g gitlab --name allow-https --description "HTTPS" --protocol tcp --priority 100 --destination-port-range "443"
az network public-ip create -n GitlabIP -g gitlab --allocation-method static --dns-name gitlabci

#set DNS record in your domain
az network dns record-set a add-record -n ci -g dns -z qubernetes.com --ipv4-address `az network public-ip list -g gitlab -o tsv|awk '{print $4}'`

wget https://gist.githubusercontent.com/ams0/304867d9a6ae7e31ef01820e0f0f8dd8/raw/abf539128ef3944a08a3414cb98ebbc411c8b77d/deploy-gitlab-ce.sh

az vm create --resource-group gitlab --name gitlab --image OpenLogic:CentOS:7-CI:latest --size Standard_DS2_v2 --admin-username centos --public-ip-address GitlabIP --nsg GitlabNSG --custom-data gitlab.sh --no-wait